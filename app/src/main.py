from PIL import Image, ExifTags
from pyspark import SparkConf
from pyspark.sql import SparkSession, Row, functions as F
import json
import boto3
import requests
import os
import glob
import logging
logging.basicConfig(level=logging.INFO)


# ## storage utils ## #
def s3_resource(url: str, user: str, psw: str):
    return boto3.resource(
        's3',
        endpoint_url=url,
        aws_access_key_id=user,
        aws_secret_access_key=psw
    )


def s3_client(url: str, user: str, psw: str):
    return boto3.client(
        's3',
        endpoint_url=url,
        aws_access_key_id=user,
        aws_secret_access_key=psw
    )


# ## image utils ## #
def download_image(url: str, target_folder: str, img_name: str) -> None:
    img_data = requests.get(url).content
    with open(os.path.join(target_folder, img_name), 'wb') as handler:
        handler.write(img_data)


def read_exif(img_path: str) -> dict:
    return {
        ExifTags.TAGS.get(tag): str(value).rstrip('\x00').strip().upper()
        for tag, value in Image.open(img_path).getexif().items()
    }


def get_name(path: str) -> str:
    return path.split('/')[-1]


if __name__ == "__main__":
    logging.info("=== data-pipeline v1.0.0 started ===\n")

    minio_url = os.getenv('MINIO_URL')
    minio_usr = os.getenv('MINIO_ROOT_USER')
    minio_psw = os.getenv('MINIO_ROOT_PASSWORD')
    minio_bucket = os.getenv('MINIO_BUCKET')
    mysql_url = os.getenv('MYSQL_URL')
    mysql_usr = os.getenv('MYSQL_ROOT_USER')
    mysql_psw = os.getenv('MYSQL_ROOT_PASSWORD')
    mysql_db = os.getenv('MYSQL_DB')
    img_urls = os.getenv('IMAGES').split(';')

    # connect to minio
    s3 = s3_resource(
        url=minio_url,
        user=minio_usr,
        psw=minio_psw
    )
    logging.info("connected to storage")

    # create fresh bucket (delete it if already existing)
    logging.info("checking buckets...")
    if minio_bucket in [bucket.name for bucket in s3.buckets.all()]:
        logging.info(f"\tfound existing bucket: {minio_bucket}")
        logging.info(f"\tclening bucket: {minio_bucket}")
        bucket = s3.Bucket(minio_bucket)
        bucket.objects.all().delete()
        bucket.delete()
    logging.info(f"\tcreating bucket: {minio_bucket}")
    s3.create_bucket(Bucket=minio_bucket)
    bucket = s3.Bucket(minio_bucket)
    logging.info("\t...done")

    # 2. Create a script to download a series of images from internet (e.g. instagram)
    logging.info("Stage 2: Create a script to download a series of images from internet (e.g. instagram)")
    for img_url in img_urls:
        download_image(url=img_url, target_folder='data', img_name=get_name(img_url))
        logging.info(f"\tdownloaded image: {get_name(img_url)}")

    # 3. Load the images in the storage (minio)
    logging.info("Stage 3:  Load the images in the storage (minio)")
    all_exif = []
    for img in glob.glob("data/*.jpg"):
        logging.info(f"\tuploading image: {get_name(img)}")
        bucket.upload_file(img, get_name(img))

        img_exif = read_exif(img)
        logging.info(f"\treading image exif: {img_exif}")

        all_exif.append({'image_name': get_name(img), 'image_exif': img_exif})
        logging.info("\t---")

    # 4. Read image EXIF and save the output on the storage (minio)
    #    Data format allowed for stage 4 are json, csv and parquet
    logging.info("Stage 4:  Read image EXIF and save the output on the storage (minio)")
    img_exif_json = json.dumps({'dataset': all_exif})
    logging.info("\tloading dataset...")
    s3.Object(minio_bucket, 'all_exif.json').put(Body=img_exif_json)
    logging.info("\t...done")

    # 5. Read the output of previous stage and write to db (mysql)
    logging.info("Stage 5: Read the output of previous stage and write to db (mysql)")
    s3 = s3_client(
        url=minio_url,
        user=minio_usr,
        psw=minio_psw
    )
    res = s3.select_object_content(
        Bucket=minio_bucket,
        Key='all_exif.json',
        ExpressionType='SQL',
        Expression="select * from s3object",
        InputSerialization={'JSON': {'Type': 'Document'}},
        OutputSerialization={'JSON': {}},
    )

    logging.info("\tconnecting pyspark to MySQL")
    conf = SparkConf() \
        .setAppName("stages56") \
        .set('spark.jars.packages', 'mysql:mysql-connector-java:8.0.22')
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    for event in res['Payload']:
        if 'Records' in event:
            records = json.loads(event['Records']['Payload'].decode('utf-8'))
            logging.info(f"\tgot dataset: {records}")

            df = spark.createDataFrame([
                    Row(name=image["image_name"], tag=k, content=v)
                    for image in records["dataset"] for k, v in image["image_exif"].items()
            ])
            # logging.info(f"\tinto dataframe: {df.show()}")

            # write mysql
            df.select("name", "tag", "content").write.format("jdbc") \
                .option("url", f"jdbc:mysql://{mysql_url}/{mysql_db}") \
                .option("driver", "com.mysql.cj.jdbc.Driver") \
                .option("dbtable", "examples") \
                .option("user", mysql_usr) \
                .option("password", mysql_psw) \
                .mode("append").save()
            logging.info("\tadded to MySQL table")

        elif 'Stats' in event:
            statsDetails = event['Stats']['Details']
            logging.info(f"\tstats details bytesScanned: {statsDetails['BytesScanned']}")
            logging.info(f"\tstats details bytesProcessed: {statsDetails['BytesProcessed']}")
        logging.info("\t---")

    # 6. Group the EXIF and create a json file with the summary (i.e. count over EXIF keys)
    logging.info("Stage 6: Group the EXIF and create a json file with the summary (i.e. count over EXIF keys)")
    df1 = spark.read.format("jdbc") \
        .option("url", f"jdbc:mysql://{mysql_url}/{mysql_db}") \
        .option("driver", "com.mysql.cj.jdbc.Driver") \
        .option("dbtable", "(select tag, content, count(*) as nb from examples group by tag, content) grouped_table") \
        .option("user", mysql_usr) \
        .option("password", mysql_psw) \
        .load()
    # logging.info(f"\tloaded grouped dataframe: {df1.show()}")

    logging.info("\tmanipulating dataframe...")
    df2 = df1 \
        .groupBy('tag') \
        .agg(F.collect_list('content'), F.collect_list('nb')) \
        .orderBy('tag')

    df3 = df2.rdd \
        .map(lambda x: (x[0], [{k: v} for k, v in zip(x[1], x[2])])) \
        .toDF(["tag", "group"])

    output = {elem['tag']: elem['group'] for elem in map(lambda row: row.asDict(), df3.collect())}
    logging.info(f"Output: {output}")

    with open("data/output.json", "w") as target_json:
        target_json.write(json.dumps(output, indent=4))
