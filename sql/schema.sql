drop table if exists examples;

CREATE TABLE examples (
  id  MEDIUMINT NOT NULL AUTO_INCREMENT,
  name varchar(80) default null,
  tag varchar(100) default null,
  content varchar(100) default null,
  primary key (id)
);