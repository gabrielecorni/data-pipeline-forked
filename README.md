# Data Pipeline

## How-to

1. Clone this repo (`main` branch)
2. ```cd data-pipeline-forked```
3. Run
    + ```docker compose up database -d```
    + ```docker compose up storage -d```
    + ```docker compose up pipeline```
4. Check the execution logs; output is also dumped to ```data/output.json``` (mounted as external volume)
5. ```docker compose down```

## Purpose

This test is designed to showcase your understanding of databases and data processing, together with your aptitude in a programming language of your choice.

## Prerequisites

- Knowledge of relational databases, including how to create tables, insert data, and query data. For the purpose of this test, we are using MySQL.
- Knowledge of a programming language, including how to read and write files, process data, and access a MySQL database.
- Familiarity with Docker for container management, which we use through the Docker Compose tool. You will need Docker and Docker Compose installed on your development machine.
- Familiarity with Git for source control, and a github.com account which will be used for sharing your code.


## Background

We have provided a Github repo containing:

- A **docker compose.yml** file that configures a container for the MySQL database, and the storage. Using this compose is not stricly mandatory.
- An **sql/schema.sql** file containing a table schema to be modify.
- A **data** folder containing:
  - a **sample_output.json** Sample output file, to show what your output should look like.

## Problem

There are a sequence of steps that we would like you to complete.

1. Fork the git repo to your own Github/Gitlab account.
2. Create a script to download a series of images from internet (e.g. instagram)
3. Load the images in the storage (minio)
4. Read image EXIF and save the output on the storage (minio)
5. Read the output of previous stage and write to db (mysql)
6. Group the EXIF and create a json file with the summary (i.e. count over EXIF keys)
7. Bonus point: define a gitlab-ci for build your code and testing
8. Share a link to your cloned github repo with us so we can review your code ahead of your interview.

### Notes on completing these tasks

- There is no right way to do this. We are interested in the choices that you make, how you justify them, and your development process. However we defined some constraints:
  - different stage of the ETL should be conteneraized (i.e. define a Dockerfile)
  - data should be storage on minio using the s3 interface
  - data format allowed for stage 4 are json, csv and parquet
  - stage 5 and 6 should be performed in spark or pyspark
- When you create a container, make sure that you add the container config to the docker compose.yml file, and add your Dockerfile and code to the images folder.
- Make sure that your code is executable, and if you are working in a scripting language, make sure that your script has an appropriate “hash-bang” line (as featured in our example scripts).
- Consider what kind of error handling and testing is appropriate.
- The MySQL database storage is ephemeral; it will not persist, so make sure all schema and data queries are repeatable.

## Notes on using the images in the git repo

### Requirements

Make sure you have recent versions of Docker and Docker Compose.

### Building the images

This will build all of the images referenced in the Docker Compose file. You will need to re-run this after making code changes. (You can also specify individual services to build if that is more convenient.)

```
docker compose build
```

### Starting MySQL

To start up the MySQL database. This will will take a short while to run the database’s start-up scripts.

```
docker compose up database
```

### Starting Minio

To start up the Minio storage. 

```
docker compose up storage
```

### Cleaning up

To tidy up, bringing down all the containers and deleting them.

```
docker compose down
```